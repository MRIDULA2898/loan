package com.customer.loanexp.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoanReqModify {
    @Min(value = 600,message = "Loan amout must be in the limit of 600 JOD to 2000 JOD") //validation for loanamount
    @Max(value = 2000,message = "Loan amout must be in the limit of 600 JOD to 2000 JOD")
    private float loanAmount;
    private int tenure;
}


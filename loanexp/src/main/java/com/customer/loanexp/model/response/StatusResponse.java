package com.customer.loanexp.model.response;

import com.customer.loanexp.constants.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StatusResponse {
        private String id;
        private StatusEnum statusEnum;

    }
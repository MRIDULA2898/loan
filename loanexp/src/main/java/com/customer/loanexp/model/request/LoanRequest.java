package com.customer.loanexp.model.request;

import com.customer.loanexp.constants.SalaryRangeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanRequest {
//validation/condition should be given in experience
        private String id;
        @Size(min = 5 ,max = 30,message = "Enter Name with size between 5 & 30") //validation/condition- it must be give above the condition needed parameter(name)
        private String name;
        private int age;
        @Pattern(regexp = "^\\d{10}$",message = "The National Id must be 10 digits")
        private String nationalId;
        @Min(value = 600,message = "Loan amout must be in the limit of 600 JOD to 2000 JOD") //validation for loanamount
        @Max(value = 2000,message = "Loan amout must be in the limit of 600 JOD to 2000 JOD")
        private float loanAmount;
        private int tenure;
        private SalaryRangeEnum salaryRangeEnum;
    }



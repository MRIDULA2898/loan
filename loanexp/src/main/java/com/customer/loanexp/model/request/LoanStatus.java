package com.customer.loanexp.model.request;

import com.customer.loanexp.constants.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor

public class LoanStatus {

        private String id;
        private StatusEnum statusEnum;
    }


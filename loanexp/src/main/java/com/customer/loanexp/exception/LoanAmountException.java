package com.customer.loanexp.exception;

public class LoanAmountException extends RuntimeException {  //whatever we given inside the runtime exception within the super cls constructor throws the given msg as a error msg
    public LoanAmountException(){
        super("Loan amount should be in between 600JOD-2000JOD");
    }
}
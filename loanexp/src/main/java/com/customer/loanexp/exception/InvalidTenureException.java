package com.customer.loanexp.exception;

public class InvalidTenureException extends  RuntimeException{//whatever we given inside the runtime exception within the super cls constructor throws the given msg as a error msg
    public InvalidTenureException(){
        super("Invalid tenure is given");
    }
}

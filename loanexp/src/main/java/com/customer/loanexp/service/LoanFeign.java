package com.customer.loanexp.service;

import com.customer.loanexp.constants.CountryEnum;
import com.customer.loanexp.model.request.LoanReqModify;
import com.customer.loanexp.model.request.LoanRequest;
import com.customer.loanexp.model.request.LoanStatus;
import com.customer.loanexp.model.request.LoanUpdate;
import com.customer.loanexp.model.response.LoanResModify;
import com.customer.loanexp.model.response.LoanResponse;
import com.customer.loanexp.model.response.StatusResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@FeignClient(name = "LoanApplication", url = "${config.rest.service.getLoanUrl}")
public interface LoanFeign {
    @PostMapping("/loan")
    public LoanResponse addLoan(@RequestBody LoanRequest loanRequest);
    @PutMapping("/loanUpdation")
     public LoanResponse updateLoan(@RequestBody LoanUpdate loanUpdate);
     @PutMapping("/loanStatus")
       public LoanResponse loanConfirm(@RequestBody LoanStatus loanStatus);

//after updation
    @GetMapping("/gettenure")
    public ArrayList<String> showMonths(@RequestHeader("country") CountryEnum countries, @RequestHeader("uuid") String uuid);
    //same from controller in mngmnt & no return bcz in feign we donot return

    //post-details
    @PostMapping("/computations")
    public LoanResModify giveDetails(@RequestHeader("country") CountryEnum countries, @RequestHeader("uuid") String uuid, @RequestBody LoanReqModify loanReqModify);

    //post-submission
    @PostMapping("/status-viewer")
    public StatusResponse displayStatus(@RequestHeader("country") CountryEnum countries, @RequestHeader("uuid") String uuid, @RequestBody LoanRequest loanRequest);

    }

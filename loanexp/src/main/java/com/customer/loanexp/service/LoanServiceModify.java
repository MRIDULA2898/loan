package com.customer.loanexp.service;

import com.customer.loanexp.constants.CountryEnum;
import com.customer.loanexp.exception.InvalidTenureException;
import com.customer.loanexp.exception.LoanAmountException;
import com.customer.loanexp.model.request.LoanReqModify;
import com.customer.loanexp.model.request.LoanRequest;
import com.customer.loanexp.model.response.LoanResModify;
import com.customer.loanexp.model.response.StatusResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
@Service
@Slf4j
public class LoanServiceModify {
    @Autowired
    LoanFeign loanFeign;

    //get
    public ArrayList<String> getMonths(CountryEnum countries, String uuid) {
        return loanFeign.showMonths(countries, uuid); //
    }

    //post-loandetails
    public LoanResModify give(LoanReqModify loanReqModify, CountryEnum countries, String uuid) {
        if (loanReqModify.getLoanAmount() > 599 || loanReqModify.getLoanAmount() < 2001) {
            tenureCheck(loanReqModify.getTenure()); //calling tenurecheck
            return loanFeign.giveDetails(countries, uuid, loanReqModify);
        } else {
            throw new LoanAmountException();
        }
    }

    //post-submission
    public StatusResponse giveStatus(CountryEnum countries, String uuid, LoanRequest loanRequest) {
        if (loanRequest.getLoanAmount() > 599 || loanRequest.getLoanAmount() < 2001) {
            tenureCheck(loanRequest.getTenure()); //calling tenurecheck
            return loanFeign.displayStatus(countries, uuid, loanRequest);
        } else {
            throw new LoanAmountException();
        }

    }



























    public void tenureCheck(int tenure){ //tenure check

        if(tenure==12 || tenure==24  || tenure== 36 || tenure==48 || tenure==60){}
        else {
            throw new InvalidTenureException();
        }
    }
}

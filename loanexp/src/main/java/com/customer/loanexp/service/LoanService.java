package com.customer.loanexp.service;

import com.customer.loanexp.exception.LoanAmountException;
import com.customer.loanexp.model.request.LoanRequest;
import com.customer.loanexp.model.request.LoanStatus;
import com.customer.loanexp.model.request.LoanUpdate;
import com.customer.loanexp.model.response.LoanResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class LoanService {
    @Autowired
    LoanFeign loanFeign;
    public LoanResponse createApplication(LoanRequest loanRequest) {
        if (loanRequest.getLoanAmount() > 599 || loanRequest.getLoanAmount() < 2001) {
            return loanFeign.addLoan(loanRequest);
        }
        else
        {
            throw new LoanAmountException();
        }

    }
//put 1st
    public LoanResponse loanChange(LoanUpdate loanUpdate) {
        return loanFeign.updateLoan(loanUpdate);

    }
//put2
    public LoanResponse status(LoanStatus loanStatus) {
        return loanFeign.loanConfirm(loanStatus);//return things in loanstatus cls
    }
}





























package com.customer.loanexp.controller;

import com.customer.loanexp.exception.LoanAmountException;
import com.customer.loanexp.model.request.LoanRequest;
import com.customer.loanexp.model.request.LoanStatus;
import com.customer.loanexp.model.request.LoanUpdate;
import com.customer.loanexp.model.response.LoanResponse;
import com.customer.loanexp.model.response.StatusResponse;
import com.customer.loanexp.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
public class LoanController {
@Autowired
    LoanService loanService;
@PostMapping
    public ResponseEntity<LoanResponse>add(@RequestBody LoanRequest loanRequest) {
        return new ResponseEntity<>(loanService.createApplication(loanRequest), HttpStatus.OK);
}
    @PutMapping("/loanUpdation")
    public ResponseEntity<LoanResponse> loanUpdating(@RequestBody LoanUpdate loanUpdate){
        return new ResponseEntity<>(loanService.loanChange(loanUpdate),HttpStatus.OK);
    }

    @PutMapping("/loanStatus")
    public ResponseEntity<StatusResponse> statusLoan(@RequestBody LoanStatus loanStatus){
        return new ResponseEntity(loanService.status(loanStatus),HttpStatus.OK);
    }
}

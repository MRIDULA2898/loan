package com.customer.loanexp.controller;

import com.customer.loanexp.constants.CountryEnum;
import com.customer.loanexp.exception.InvalidTenureException;
import com.customer.loanexp.model.request.LoanReqModify;
import com.customer.loanexp.model.request.LoanRequest;
import com.customer.loanexp.model.response.LoanResModify;
import com.customer.loanexp.model.response.StatusResponse;
import com.customer.loanexp.service.LoanServiceModify;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

@RestController
@RequestMapping
public class LoanControllerModify {
    @Autowired
    LoanServiceModify loanServiceModify;

//get-retriving
    @GetMapping("/getTenure")       //(here we donot need @valid bcz it get on months,no name & loanamount posting or getting)
    public ArrayList<String> showMonths( @RequestHeader("country") CountryEnum countries, @RequestHeader("uuid") String uuid)//showMonths method for feign connection,that must give in service too
    {
        return loanServiceModify.getMonths(countries,uuid);
    }


//post details(loana/m,tenure--> get loandetails)
    @PostMapping("/computations")                   //@Valid is for validation(validation for name & loanAmount)
    public ResponseEntity<?> giveDetails(@RequestHeader("country") CountryEnum countries, @RequestHeader("uuid") String uuid, @RequestBody @Valid  LoanReqModify loanReqModify){
        try {
            return new ResponseEntity(loanServiceModify.give(loanReqModify, countries, uuid), HttpStatus.OK); //if it is valid this will execute
        }
        catch (InvalidTenureException i){ //else it return this bad request
            return new ResponseEntity<>(i.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }

    //post-submission
    @PostMapping("/status-viewer")
    public ResponseEntity<?> displayStatus( @RequestHeader("country") CountryEnum countries, @RequestHeader("uuid") String uuid, @RequestBody @Valid  LoanRequest loanRequest){
        try {
            return new ResponseEntity(loanServiceModify.giveStatus(countries, uuid, loanRequest), HttpStatus.OK);
        }
        catch (InvalidTenureException i){
            return new ResponseEntity<>(i.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }


}

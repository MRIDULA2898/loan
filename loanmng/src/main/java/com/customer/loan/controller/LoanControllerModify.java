package com.customer.loan.controller;

import com.customer.loan.Constants.CountryEnum;
import com.customer.loan.model.request.LoanReqModify;
import com.customer.loan.model.request.LoanRequest;
import com.customer.loan.model.response.LoanResModify;
import com.customer.loan.model.response.LoanResponse;
import com.customer.loan.model.response.StatusResModify;
import com.customer.loan.model.response.StatusResponse;
import com.customer.loan.service.LoanServiceModify;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Month;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping
public class LoanControllerModify {
    @Autowired
    LoanServiceModify loanServiceModify;

    @GetMapping("/gettenure")
    public ArrayList<String> showMonths(@RequestHeader("country") CountryEnum countries, @RequestHeader("uuid") String uuid)
    {
        return loanServiceModify.getMonths(countries,uuid);
    }

    @PostMapping("/computations")
    public ResponseEntity<LoanResModify> giveDetails(@RequestHeader("country") CountryEnum countries, @RequestHeader("uuid") String uuid,@RequestBody LoanReqModify loanReqModify){
        return new ResponseEntity(loanServiceModify.give(loanReqModify,countries,uuid), HttpStatus.OK); //req loanReqModify, response loanresModify
    }

    @PostMapping("/status-viewer")
    public ResponseEntity<StatusResponse> displayStatus(@RequestHeader("country") CountryEnum countries, @RequestHeader("uuid") String uuid,@RequestBody LoanRequest loanRequest){
        return new ResponseEntity(loanServiceModify.giveStatus(countries,uuid,loanRequest),HttpStatus.OK);
    }

}

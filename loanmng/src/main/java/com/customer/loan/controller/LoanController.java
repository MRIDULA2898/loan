package com.customer.loan.controller;

import com.customer.loan.model.repository.LoanRepo;
import com.customer.loan.model.request.LoanRequest;
import com.customer.loan.model.request.LoanStatus;
import com.customer.loan.model.request.LoanUpdate;
import com.customer.loan.model.response.LoanResponse;
import com.customer.loan.model.response.StatusResponse;
import com.customer.loan.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class LoanController {
    @Autowired
    LoanService loanService;
    @PostMapping("/loan")
    public ResponseEntity<LoanResponse> add(@RequestBody LoanRequest loanRequest){
        return new ResponseEntity(loanService.add(loanRequest), HttpStatus.OK);
    }

    @PutMapping("/loanUpdation")
    public ResponseEntity<LoanResponse> loanUpdating(@RequestBody LoanUpdate loanUpdate){
        return new ResponseEntity<>(loanService.loanChange(loanUpdate),HttpStatus.OK);
    }

    @PutMapping("/loanStatus")
    public ResponseEntity<StatusResponse> loanStatus(@RequestBody LoanStatus loanStatus){
        return new ResponseEntity<>(loanService.status(loanStatus),HttpStatus.OK);
    }

}


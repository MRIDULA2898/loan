package com.customer.loan.service;

import com.customer.loan.model.entity.LoanEntity;
import com.customer.loan.model.repository.LoanRepo;
import com.customer.loan.model.request.LoanRequest;
import com.customer.loan.model.request.LoanStatus;
import com.customer.loan.model.request.LoanUpdate;
import com.customer.loan.model.response.LoanResponse;
import com.customer.loan.model.response.StatusResponse;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.customer.loan.Constants.ComputationConstant.*;

@Service
@Slf4j
public class LoanService {
    @Autowired
    LoanRepo loanRepo;
    @Autowired
    ModelMapper mapper;
    @Autowired
    ComputationService computationService;

    public LoanResponse add(LoanRequest loanRequest) {
        float loanInterestRate = 0;
        float loanFees = 0;
        switch (loanRequest.getTenure()) {
            case 60:
                loanInterestRate = SIXTY_RATE;
                loanFees = SIXTY_FEES;
                break;
            case 48:
                loanInterestRate = FORTY_RATE;
                loanFees = FORTY_FEES;
                break;
            case 36:
                loanInterestRate = THIRTY_RATE;
                loanFees = THIRTY_FEES;
                break;
            case 24:
                loanInterestRate = TWENTY_RATE;
                loanFees = TWENTY_FEES;
                break;
            case 12:
                loanInterestRate = TWELVE_RATE;
                loanFees = TWELVE_FEES;
                break;

        }
        int loanInstallment = computationService.calculateInstallment(loanRequest.getLoanAmount(), loanInterestRate, loanRequest.getTenure());
        int loanAmount = loanInstallment * loanRequest.getTenure();
        int loanTotalInterest = (int) Math.round(loanAmount - loanRequest.getLoanAmount());
        log.info(String.valueOf(loanTotalInterest) + loanRequest.getTenure());
        LoanResponse loanResponse = new LoanResponse(null, loanInstallment, loanInterestRate, loanTotalInterest, loanFees, (loanAmount + loanFees));

        LoanEntity loanEntity = new LoanEntity();
        loanEntity.setId(loanRequest.getId());
        loanEntity.setName(loanRequest.getName());
        loanEntity.setAge(loanRequest.getAge());
        loanEntity.setLoanAmount(loanRequest.getLoanAmount());
        loanEntity.setNationalId(loanRequest.getNationalId());
        loanEntity.setTenure(loanRequest.getTenure());
        loanEntity.setInstallment(loanResponse.getInstallment());
        loanEntity.setInterest(loanResponse.getInterest());
        loanEntity.setTotalInterestAmt(loanResponse.getTotalInterestAmt());
        loanEntity.setFee(loanResponse.getFee());
        loanEntity.setTotal(loanResponse.getTotal());
        loanRepo.save(loanEntity);
        return loanResponse;
    }
//put
    public LoanResponse loanChange(LoanUpdate loanUpdate) {

        float loanInterestRate = 0;
        float loanFees = 0;
        switch (loanUpdate.getTenure()) {
            case 60:
                loanInterestRate = SIXTY_RATE;
                loanFees = SIXTY_FEES;
                break;
            case 48:
                loanInterestRate = FORTY_RATE;
                loanFees = FORTY_FEES;
                break;
            case 36:
                loanInterestRate = THIRTY_RATE;
                loanFees = THIRTY_FEES;
                break;
            case 24:
                loanInterestRate = TWENTY_RATE;
                loanFees = TWENTY_FEES;
                break;
            case 12:
                loanInterestRate = TWELVE_RATE;
                loanFees = TWELVE_FEES;
                break;

        }
        LoanEntity loanEntity = loanRepo.findById(loanUpdate.getId()).orElseThrow(() -> new RuntimeException("not found"));
        int loanInstallment = computationService.calculateInstallment(loanEntity.getLoanAmount(), loanInterestRate, loanUpdate.getTenure());
        int loanAmount = loanInstallment * loanUpdate.getTenure();
        int loanTotalInterest = (int) Math.round(loanAmount - loanEntity.getLoanAmount());
        log.info(String.valueOf(loanTotalInterest) + loanUpdate.getTenure());
        LoanResponse loanResponse = new LoanResponse(null, loanInstallment, loanInterestRate, loanTotalInterest, loanFees, (loanAmount + loanFees));
        loanEntity.setInstallment(loanResponse.getInstallment());
        loanEntity.setInterest(loanResponse.getInterest());
        loanEntity.setTotalInterestAmt(loanResponse.getTotalInterestAmt());
        loanEntity.setFee(loanResponse.getFee());
        loanEntity.setTotal(loanResponse.getTotal());
        loanEntity.setTenure(loanUpdate.getTenure());
        loanRepo.save(loanEntity);
        return loanResponse;
    }

    //put -submission
    public StatusResponse status(LoanStatus loanStatus) {
        LoanEntity loanEntity = loanRepo.findById(loanStatus.getId()).orElseThrow(() -> new RuntimeException("not found"));// for getting id from mongo db
        StatusResponse statusResponse = new StatusResponse(loanEntity.getId(), loanStatus.getStatusEnum());
        loanEntity.setStatusEnum(loanStatus.getStatusEnum());
        loanRepo.save(loanEntity);
        return statusResponse;
    }
}

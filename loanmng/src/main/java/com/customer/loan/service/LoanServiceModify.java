package com.customer.loan.service;

import com.customer.loan.Constants.CountryEnum;
import com.customer.loan.Constants.StatusEnum;
import com.customer.loan.model.entity.LoanEntity;
import com.customer.loan.model.repository.LoanRepo;
import com.customer.loan.model.request.LoanReqModify;
import com.customer.loan.model.request.LoanRequest;
import com.customer.loan.model.response.LoanResModify;
import com.customer.loan.model.response.LoanResponse;
import com.customer.loan.model.response.StatusResModify;
import com.customer.loan.model.response.StatusResponse;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestHeader;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static com.customer.loan.Constants.ComputationConstant.*;
import static com.customer.loan.Constants.ComputationConstant.TWELVE_FEES;

@Service
@Slf4j
public class LoanServiceModify<Months, months> {
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    ComputationService computationService;
    @Autowired
    LoanRepo loanRepo;

    //get-retrive
    public ArrayList<String> getMonths( CountryEnum countries,String uuid) {
        ArrayList<String> month = new ArrayList<>();
        month.add("Months-60");
        month.add("Months-48");
        month.add("Months-36");
        month.add("Months-24");
        month.add("Months-12");
        return month;
    }

    //post
    public LoanResModify give(LoanReqModify loanReqModify,CountryEnum countries,String uuid) {
        float loanInterestRate = 0;
        float loanFees = 0;
        switch (loanReqModify.getTenure()) {
            case 60:
                loanInterestRate = SIXTY_RATE;
                loanFees = SIXTY_FEES;
                break;
            case 48:
                loanInterestRate = FORTY_RATE;
                loanFees = FORTY_FEES;
                break;
            case 36:
                loanInterestRate = THIRTY_RATE;
                loanFees = THIRTY_FEES;
                break;
            case 24:
                loanInterestRate = TWENTY_RATE;
                loanFees = TWENTY_FEES;
                break;
            case 12:
                loanInterestRate = TWELVE_RATE;
                loanFees = TWELVE_FEES;
                break;

        }
        int loanInstallment = computationService.calculateInstallment(loanReqModify.getLoanAmount(), loanInterestRate, loanReqModify.getTenure());
        int loanAmount = loanInstallment * loanReqModify.getTenure();
        float total = loanAmount + loanFees;
        int loanTotalInterest = (int) Math.round(loanAmount - loanReqModify.getLoanAmount());
        log.info(String.valueOf(loanTotalInterest) + loanReqModify.getTenure());
        LoanResModify loanResModify = new LoanResModify(loanReqModify.getLoanAmount(), loanReqModify.getTenure(), loanInstallment, loanInterestRate, loanTotalInterest, loanFees, total);
        return loanResModify;
    }
    //post

    public StatusResponse giveStatus(@RequestHeader("country") CountryEnum countries, @RequestHeader("uuid") String uuid,LoanRequest loanRequest) {
        float loanInterestRate = 0;
        float loanFees = 0;
        switch (loanRequest.getTenure()) {
            case 60:
                loanInterestRate = SIXTY_RATE;
                loanFees = SIXTY_FEES;
                break;
            case 48:
                loanInterestRate = FORTY_RATE;
                loanFees = FORTY_FEES;
                break;
            case 36:
                loanInterestRate = THIRTY_RATE;
                loanFees = THIRTY_FEES;
                break;
            case 24:
                loanInterestRate = TWENTY_RATE;
                loanFees = TWENTY_FEES;
                break;
            case 12:
                loanInterestRate = TWELVE_RATE;
                loanFees = TWELVE_FEES;
                break;

        }
        int loanInstallment = computationService.calculateInstallment(loanRequest.getLoanAmount(), loanInterestRate, loanRequest.getTenure());
        int loanAmount = loanInstallment * loanRequest.getTenure();
        float total = loanAmount + loanFees;
        int loanTotalInterest = (int) Math.round(loanAmount - loanRequest.getLoanAmount());
        log.info(String.valueOf(loanTotalInterest) + loanRequest.getTenure());
        LoanEntity loanEntity = modelMapper.map(loanRequest, LoanEntity.class);//loanRequest is source,LoanEntity is destination,before setting and getting we hv to map using ModelMapper obj(modelMapper),(LoanEntity loanEntity-bcz we are giving evythng to LoanEntity(destination)
        loanEntity.setId(null);//set id in entity as null
        loanEntity.setName(loanRequest.getName());//taking Name from loanrequest and give it to loanEntity
        loanEntity.setAge(loanRequest.getAge());
        loanEntity.setNationalId(loanRequest.getNationalId());
        loanEntity.setLoanAmount(loanRequest.getLoanAmount());
        loanEntity.setTenure(loanRequest.getTenure());
        loanEntity.setInstallment(loanInstallment);
        loanEntity.setInterest(loanInterestRate);
        loanEntity.setTotalInterestAmt(loanTotalInterest);
        loanEntity.setFee(loanFees); //no loanfee in loanrequest but this calculated above this just need to set
        loanEntity.setTotal(total);
        loanEntity.setCreatedDate(LocalDateTime.now());
        loanEntity.setCreatedBy(loanEntity.getName());//CreatedBy- gives who create application,it gets the name of customername(create application) from entity
        loanEntity.setStatusEnum(StatusEnum.CONFIRMED);
        LoanEntity display = loanRepo.save(loanEntity);//now evrything comes in loanentity save that to repository
        return new StatusResponse(display.getId(),display.getStatusEnum());//displaying:taking id,statusenum from statusResponse and return

// in postgres we have to use long datatype for id
    }

    }

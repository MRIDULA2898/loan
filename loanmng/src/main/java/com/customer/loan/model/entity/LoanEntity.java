package com.customer.loan.model.entity;

import com.customer.loan.Constants.CountryEnum;
import com.customer.loan.Constants.SalaryRangeEnum;
import com.customer.loan.Constants.StatusEnum;
import com.customer.loan.model.request.LoanRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "Loan-Application")

public class LoanEntity {
    @Id
    private String id;
    private String name;
    private int age;
    private String nationalId;
    private float loanAmount;
    private int tenure;

    private float installment;
    private float interest;
    private float totalInterestAmt;
    private float fee;
    private float total;
    private SalaryRangeEnum salaryRangeEnum;
    private StatusEnum statusEnum;

    private LocalDateTime createdDate; //to get date&time of Submission of application(submit time)
    private CountryEnum countryEnum;
    private String uuid;
    private String createdBy;
}

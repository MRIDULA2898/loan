package com.customer.loan.model.request;

import com.customer.loan.Constants.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanStatus {

    private String id;
    private StatusEnum statusEnum;
}

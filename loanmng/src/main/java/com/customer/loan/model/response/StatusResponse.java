package com.customer.loan.model.response;

import com.customer.loan.Constants.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StatusResponse {
    private String id;
    private StatusEnum statusEnum;

}

package com.customer.loan.model.request;


import com.customer.loan.Constants.SalaryRangeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanRequest {
    private String id;
    private String name;
    private int age;
    private String nationalId;
    private float loanAmount;
    private int tenure;
    private SalaryRangeEnum salaryRangeEnum;
}

package com.customer.loan.model.response;

import com.customer.loan.Constants.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StatusResModify {
//    private
    private StatusEnum statusEnum;
}

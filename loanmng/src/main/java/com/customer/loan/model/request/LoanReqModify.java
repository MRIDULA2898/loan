package com.customer.loan.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoanReqModify {
    private float loanAmount;
    private int tenure;
}
